# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit rpm

DESCRIPTION="A free version of Astah UML editor"
HOMEPAGE="http://astah-users.change-vision.com/en/"
SRC_URI="http://cdn.change-vision.com/files/${PN}-community-${PV}-0.noarch.rpm"

LICENSE=""
SLOT="0"
KEYWORDS="~x86 ~amd64"
DEPEND="virtual/jre"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install(){
	insinto /usr
	doins -r usr/*
	chmod +x "${D}/usr/lib/astah_community/astah-com"
}
