EAPI=4
inherit eutils
#PVERSION='8.0.3'
EAP=""
#EAP="-6-Preview"

DESCRIPTION="The intelligent cross-platform C/C++ IDE"
HOMEPAGE="http://jetbrains.com/clion/"
SRC_URI="http://download.jetbrains.com/cpp/${P}.tar.gz"
LICENSE="Apache-2.0"
IUSE=""

KEYWORDS="x86 amd64"

PROGNAME="clion"

RESTRICT="strip mirror"
DEPEND=">=virtual/jre-1.6"
SLOT="0"
S=${WORKDIR}

src_fetch() {
echo $P
}

src_install() {
	dodir /opt/${PN}

	cd clion*/
	sed -i 's/IS_EAP="true"/IS_EAP="false"/' bin/clion.sh
	insinto /opt/${PN}
	doins -r *

	fperms a+x /opt/${PN}/bin/clion.sh || die "Chmod failed"
	fperms a+x /opt/${PN}/bin/fsnotifier || die "Chmod failed"
	fperms a+x /opt/${PN}/bin/fsnotifier64 || die "Chmod failed"
	dosym /opt/${PN}/bin/clion.sh /usr/bin/${PN}
	
	#mv "bin/clion.svg" "bin/${PN}.svg"
	doicon "bin/${PN}.svg"
	make_desktop_entry ${PN} "${PROGNAME}" "${PN}"
}

pkg_postinst() {
    elog "Run /usr/bin/${PN}"
}


