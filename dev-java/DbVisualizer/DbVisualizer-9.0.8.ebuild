# Copyright 1999-2006 Gentoo Technologies, Inc.
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils
DESCRIPTION="Minq Software's DB Visualizer (Free Version)"
HOMEPAGE="http://www.dbvis.com/products/dbvis/download/"
SRC_URI="http://www.dbvis.com/product_download/dbvis-${PV}/media/dbvis_unix_9_0_8.tar.gz"

SLOT="0"
KEYWORDS="~x86 ~amd64"
LICENSE="Apache-1.1"

RDEPEND="virtual/jre"
DEPEND="${RDEPEND}"
S="${WORKDIR}/${PN}"

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/${PV}-dbvis.patch
}

src_install () {
	insinto /opt/${P}
	doins -r .install4j *
	newins ${FILESDIR}/${PV}-dbvis.png dbvis.png
	dodir /opt/${P}/jdbc
	keepdir /opt/${P}/jdbc

	insinto /usr/share/applications
	newins ${FILESDIR}/${PV}-dbvis.desktop ${P}.desktop

	fperms 0755 /opt/${P}/dbvis
	dosym /opt/${P}/dbvis /usr/bin/dbvis
}

