# Copyright 1999-2014ntoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit autotools subversion

DESCRIPTION="Tagsistant semantic filesystem"
HOMEPAGE="http://tagsistant.net"
ESVN_REPO_URI="http://svn.gna.org/svn/tagfs/branches/${PV}"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-db/libdbi
	>=media-libs/libextractor-0.5"
RDEPEND="${DEPEND}
	sys-devel/automake:1.14"

src_prepare(){
	eautoconf
	eautomake
}
