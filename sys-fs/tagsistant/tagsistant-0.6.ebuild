# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
#inherit subversion

DESCRIPTION=""
HOMEPAGE="http://tagsistant.net"
SRC_URI="http://www.tagsistant.net/download/${PN}-0-6/finish/4-${PN}-0-6/10-${PN}-0-6/0 -> ${P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="dev-db/libdbi"
RDEPEND="${DEPEND}"

