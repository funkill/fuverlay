# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit rpm
DESCRIPTION="Audio/Visual control of your system"
HOMEPAGE="http://selina-corp.com/aais"
SRC_URI="http://selina-corp.com/alenaais/install/AlenaAIS.rpm"

LICENSE="HZ"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="media-sound/sox
	dev-lang/python:2.7[tk]
	dev-python/PyQt4
	media-libs/opencv
	x11-misc/xdotool"
RDEPEND="${DEPEND}"
S="${WORKDIR}"

src_install(){
	insinto /opt
	doins -r opt/*
	insinto /usr
	doins -r usr/*
	chmod a+rwX -R "${D}/opt/AlenaAIS/cache"
	chmod a+x -R "${D}opt/AlenaAIS/sFunc/moduls_aais" "${D}opt/AlenaAIS/sFunc/python_core" "${D}/opt/AlenaAIS/AlenaAIS.py"
	einfo "Use program only being in the directory with the installed program!"
}
