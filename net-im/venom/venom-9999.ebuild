# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit cmake-utils git-r3

DESCRIPTION="A Vala/Gtk+ graphical user interface for Tox"
HOMEPAGE="https://github.com/naxuroqa/Venom"

EGIT_REPO_URI="https://github.com/naxuroqa/Venom.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="qrencode libnotify"

DEPEND="
	>=dev-lang/vala-0.18.0
	dev-util/cmake
	net-im/tox-core
	x11-libs/gtk+:3
	dev-db/sqlite:3
	dev-libs/json-glib
	dev-libs/glib:2
	qrencode? ( media-gfx/qrencode )
	libnotify? ( x11-libs/libnotify )"
RDEPEND="${DEPEND}"

src_configure(){
	mycmakeargs=(
		$(cmake-utils_use_enable qrencode QR_ENCODE)
		$(cmake-utils_use_enable libnotify LIBNOTIFY)
	)
	cmake-utils_src_configure
}
