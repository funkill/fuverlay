# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit cmake-utils git-r3

DESCRIPTION="A Vala/Gtk+ graphical user interface for Tox"
HOMEPAGE="https://github.com/naxuroqa/Venom"

EGIT_REPO_URI="https://github.com/naxuroqa/Venom.git"
EGIT_COMMIT="${PV}"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND="
	>=dev-lang/vala-0.17.0
	dev-util/cmake
	dev-libs/libgee:0.8
	x11-libs/gtk+:3
	net-im/tox-core"
RDEPEND="${DEPEND}"

src_install(){
	cmake-utils_src_install
	insinto /usr/share/applications/
	newins "${FILESDIR}/venom.desktop" venom.desktop
}
