# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
inherit rpm bash-completion-r1

QA_PRESTRIPPED="/usr/bin/yandex-disk"
REV_VER="1"

DESCRIPTION="Official Yandex.Disk client"
HOMEPAGE="http://disk.yandex.com/"
SRC_URI="x86? ( http://repo.yandex.ru/yandex-disk/rpm/stable/i386/${P}-${REV_VER}.fedora.i386.rpm )
	amd64? ( http://repo.yandex.ru/yandex-disk/rpm/stable/x86_64/${P}-${REV_VER}.fedora.x86_64.rpm )"

LICENSE="Yandex Desktop Software License Agreement"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="+bash-completion"

DEPEND="bash-completion? ( app-shells/bash-completion )"
RDEPEND="${DEPEND}"

S=${WORKDIR}

src_install(){
	insinto /usr
	doins -r ${WORKDIR}/usr/*
	chmod a+x "${D}/usr/bin/yandex-disk"
	if use bash-completion; then
		mv "${WORKDIR}/etc/bash_completion.d/yandex-disk-completion.bash" "${WORKDIR}/etc/bash_completion.d/yandex-disk"
		dobashcomp ${WORKDIR}/etc/bash_completion.d/yandex-disk
	fi
}

