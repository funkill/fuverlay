# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-r3

DESCRIPTIO="tabbed extension for rxvt-unicode (urxvt) with several extra
features"
HOMEPAGE="http://github.com/stepb/urxvt-tabbedex"
EGIT_REPO_URI="https://github.com/stepb/urxvt-tabbedex.git"
EGIT_COMMIT="f9490d8"

LICENSE="GPL"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

DEPEND="x11-terms/rxvt-unicode[perl]"
RDEPEND="${DEPEND}"

src_install() {
	install -Dm644 ${S}/tabbedex ${D}/usr/lib/urxvt/perl/tabbedex
}
