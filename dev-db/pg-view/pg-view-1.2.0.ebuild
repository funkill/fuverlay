# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-python/into/into-0.1.3.ebuild,v 1.1 2014/12/19 08:04:39 patrick Exp $

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1
MY_PN="pg-view"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="PostgreSQL Activity View Utility"

HOMEPAGE="https://pypi.python.org/pypi/${MY_PN}/${MY_P}"

SRC_URI="mirror://pypi/${MY_PN:0:1}/${MY_PN}/${MY_P}.tar.gz"

LICENSE="Apache License 2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""
DEPEND="${RDEPEND}
	dev-python/psycopg:2
	sys-libs/ncurses
"

S="${WORKDIR}/${MY_P}"
